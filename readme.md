## Epitech TV - API GRR

### Pré-requis

* PHP >= 5.4
* Pré-requis Laravel : http://laravel.com/docs/5.0#server-requirements

---
### Installation

```
git clone git@bitbucket.org:aerstrasbourg/api-grr.git
cd api-grr
cp .env.example .env
nano .env
```

* Passer `APP_DEBUG` à `false`
* Renseigner les informations de la base de donnée de GRR

```
composer update // Téléchargement des modules de Laravel
chmod 0777 -R storage
```

---
### Utilisation

L'API permet d'utiliser 2 routes différentes :

* `GET /rooms/{for_epitech?}` : Récupère la liste des salles disponibles (avec `for_epitech` à 1 si on veut uniquement celles réservées pour Epitech ?).
Exemple de données renvoyées :
```
[
  {
    "id": "2",
    "area_id": "1",
    "room_name": "D201",
    "description": "",
    "capacity": "22",
    "max_booking": "-1",
    "statut_room": "1",
    "show_fic_room": "n",
    "picture_room": "",
    "comment_room": "",
    "show_comment": "n",
    "delais_max_resa_room": "-1",
    "delais_min_resa_room": "0",
    "allow_action_in_past": "n",
    "dont_allow_modify": "n",
    "order_display": "0",
    "delais_option_reservation": "0",
    "type_affichage_reser": "0",
    "moderate": "0",
    "qui_peut_reserver_pour": "5",
    "active_ressource_empruntee": "n",
    "who_can_see": "0",
    "for_epitech": "0"
  },
  {
    "id": "3",
    "area_id": "1",
    "room_name": "D202",
    "description": "",
    "capacity": "36",
    "max_booking": "-1",
    "statut_room": "1",
    "show_fic_room": "n",
    "picture_room": "",
    "comment_room": "",
    "show_comment": "n",
    "delais_max_resa_room": "-1",
    "delais_min_resa_room": "0",
    "allow_action_in_past": "n",
    "dont_allow_modify": "n",
    "order_display": "0",
    "delais_option_reservation": "0",
    "type_affichage_reser": "0",
    "moderate": "0",
    "qui_peut_reserver_pour": "5",
    "active_ressource_empruntee": "n",
    "who_can_see": "0",
    "for_epitech": "1"
  }
]
```
* `POST /planning` : Récupère la liste des activités dans les salles passées en paramètre pour la journée active.
Exemple de données à envoyer (à passer dans le champ `params`) :
```
{
	"rooms": ["D300","D301"]
}
```
Exemple de données renvoyées (entries contient la liste des activités par salle) :
```
[
  {
    "id": "6",
    "area_id": "1",
    "room_name": "D300",
    "description": "",
    "capacity": "25",
    "max_booking": "-1",
    "statut_room": "1",
    "show_fic_room": "n",
    "picture_room": "",
    "comment_room": "",
    "show_comment": "n",
    "delais_max_resa_room": "-1",
    "delais_min_resa_room": "0",
    "allow_action_in_past": "n",
    "dont_allow_modify": "n",
    "order_display": "0",
    "delais_option_reservation": "0",
    "type_affichage_reser": "0",
    "moderate": "0",
    "qui_peut_reserver_pour": "5",
    "active_ressource_empruntee": "n",
    "who_can_see": "0",
    "for_epitech": "1",
    "entries": [
      {
        "id": "50888",
        "start_time": "1436392800",
        "end_time": "1436477400",
        "entry_type": "1",
        "repeat_id": "3314",
        "room_id": "6",
        "timestamp": "2015-06-25 16:41:47",
        "create_by": "ISEG",
        "beneficiaire_ext": "",
        "beneficiaire": "ISEG",
        "name": "ISEG",
        "type": "A",
        "description": "Summer Code Camp",
        "statut_entry": "-",
        "option_reservation": "-1",
        "overload_desc": "",
        "moderate": "0",
        "jours": "0"
      }
    ]
  },
  {
    "id": "7",
    "area_id": "1",
    "room_name": "D301",
    "description": "",
    "capacity": "24",
    "max_booking": "-1",
    "statut_room": "1",
    "show_fic_room": "n",
    "picture_room": "",
    "comment_room": "",
    "show_comment": "n",
    "delais_max_resa_room": "-1",
    "delais_min_resa_room": "0",
    "allow_action_in_past": "n",
    "dont_allow_modify": "n",
    "order_display": "0",
    "delais_option_reservation": "0",
    "type_affichage_reser": "0",
    "moderate": "0",
    "qui_peut_reserver_pour": "5",
    "active_ressource_empruntee": "n",
    "who_can_see": "0",
    "for_epitech": "1",
    "entries": []
  }
]
```