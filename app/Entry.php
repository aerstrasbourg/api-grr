<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;

class Entry extends Model
{
	protected $table = 'grr_entry';
	public $timestamps = false;

	public function room()
	{
		return ($this->belongsTo('App\Room'));
	}

}
