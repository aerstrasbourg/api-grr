<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'cors'], function (Illuminate\Routing\Router $router)
{
	/*
	* Récupère la liste des salles et les évènements associés pour la journée
	*/
	$router->post('/planning', ['as' => 'planning.show', 'uses' => 'PlanningController@show']);

	/*
	* Récupère la liste des salles
	* @for_epitech : optionnel, 1 ou 0, selon qu'on veut récupérer seulement les salles pour Epitech ou non
	*/
	$router->get('/rooms/{for_epitech?}', ['as' => 'room.list', 'uses' => 'RoomController@showlist'])
			 ->where(['for_epitech' => '[0-1]']);
});
