<?php namespace App\Http\Controllers;

use App\Room;

class RoomController extends Controller
{

	public function __construct()
	{
	}

	public function showlist($forEpitech = NULL)
	{
		if ($forEpitech != NULL)
			return (Room::where('for_epitech', '=', $forEpitech)->get());
		else
			return (Room::get());
	}

}
