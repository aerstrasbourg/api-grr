<?php namespace App\Http\Controllers;

use Input;
use App\Room, App\Entry;

class PlanningController extends Controller
{

	public function __construct()
	{
	}

	public function show()
	{
		$params = Input::get('params');
		// Real datas or test datas...
		$params = !empty($params) ? json_decode($params) : (object)[
			'rooms' => [
				 'D300',
			],
		];

		$rooms = ["error" => "Invalid JSON."];
		if (!empty($params))
		{
			$rooms = Room::with(['entries' => function ($q){
				$q->where('start_time', '>=', mktime(0, 0, 0, getdate()['mon'], getdate()['mday'], getdate()['year']))
				  ->where('start_time', '<=', mktime(23, 42, 0, getdate()['mon'], getdate()['mday'], getdate()['year']));
			}])->whereIn('room_name', $params->rooms)->get();
		}

		return ($rooms);
	}

}
