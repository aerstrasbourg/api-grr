<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Entry;

class Room extends Model
{
	protected $table = 'grr_room';
	public $timestamps = false;

	/*
	public function entries($startTime = NULL, $endTime = NULL)
	{
		if (!$startTime || !$endTime)
			return ($this->hasMany('App\Entry', 'room_id', 'id'));
		else
			return ($this->hasMany('App\Entry', 'room_id', 'id')->where('start_time', '>=', $startTime)->where('start_time', '<=', $endTime));
	}
	*/

	public function entries()
	{
		return ($this->hasMany('App\Entry', 'room_id', 'id'));
	}

}
